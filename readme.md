Tutorial in shUnit
==================

shUnit is a framework for unit tests on operating system level. It can be used to test whether
something compiles or not, whether shell scripts do what they should, and for many other things
for sure.


Test Execution
==============

Simply execute the *./test.sh* script to execute all test cases. Each test case resides in a
subdirectory inside a test suite directory. The directory this file resides in is a test suite,
by the way. The script iterates through all subdirectories in this and tries to execute them as
test suites or test cases. If a subdirectory is indictated as a test suite or test case but lacks
approtiate files an error will be reported. Test suites and cases can be executed separately by
executing the corresponding scripts with the subdirectory as working directory.


Test Framework
==============

The framework uses a concept of test cases that test a specific aspect and test suites containing
further test suites and test cases. The test suite that is not nested in some other test suite is
the parent test suite. Each directory that is a test suite must contain a *.suite* file and each
directory that is a test case must contain a *.test* file.


Test Suites
-----------

Each test suite contains these files:

* .suite       - Indicates that the directory is a test suite.
* new_suite.sh - A script that creates nested test suites.
* new_test.sh  - A script that creates test cases.
* test.sh      - A script that executes all nested test suites and test cases.
* readme.md    - A description of what the test suite tests.
* .template/   - A directory containing a template for test cases.

Each test suite must have a *.suite* file. This is especially important for nested test suites as
the *test.sh* script interprets only such subdirectories as test suites. By using the *.suite* file
(and *.test* file in case of test cases) additional directories that shall not be test suites may
reside inside test suites.

The shell scripts should be used to create test suites and cases and to execute all tests. Nested
test suites do not contain those scripts but a soft link to these scripts in the parent test suite
(all nested test suites link recursively to the parent test suite files). With such a structure
test suites don't need a full copy of the scripts and only a couple of scripts must be changed if
needed and not all the scripts in all the test suites.

The *new_suite.sh* script creates a subdirectory for a test suite. If there is already a directory
with that name the script aborts with an error. It then creates the links to the scripts and the
*.template/* directory in the actual test suite and creates the files listed above.

The *new_test.sh* script creates a subdirectory for a test case. If there is already a directory
with that name the script aborts with an error.  It then copies the content of *.template/* to the
test case directory.

The *test.sh* script is the heart of the framework. It iterates over all subdirectories and checks
if they are test suites or test cases and if so it tries to execute them (recursively in the case
of test suites). It prints a graph of the test cases found and whether they passed, failed or were
aborted due to some error. At the end it prints a summary.


Test Cases
----------

Each test case contains these files:

* .test        - Indicates that the directory is a test case.
* init.sh      - A script that creates all necessary files for the test execution.
* run.sh       - A script that performs the actual test and exits with a success or failure.
* clean.sh     - A script to reset the test case to the pre-test state.
* on_error.txt - A short message that will be printed if the test case fails.
* readme.md    - A description of what the test case tests and in what cases it passes or fails.

Each test case must have a *.test* file as the *test.sh* script interprets only such directories
as test cases. All files are copies from the *.template/* directory unlike links as it is the case
with test suites. The reason is that all test cases will have a custom initialization, execution
and cleanup routine. You may change the scripts in the *.template* directory to create a customized
template that will be used for future test cases.

The shell scripts are used to initialize, execute and reset the test cases and must be customized.
They should exit with an error code to indicate whether they executed properly or not. If the
*run.sh* script exits with an error code the content of the *on_error.txt* file should be printed
that describes what the test case expected to order to pass.

The *readme.md* file shall explain what the test case tests and in what case it passes. It may
provide further information like a changelog, etc.
