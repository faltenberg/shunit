#!/bin/bash

# Author: [Artur Faltenberg](artur2001-program@yahoo.de)
# Date:   15.02.2016

# This script interprets this directory as a test suite and performs all test cases in the suite.
# No matter the working directory, test cases are searched in the script's home directory. If a
# link to this script is executed, the link's home directory acts as the test suite and not the
# directory of this original script file.
#
# The script returns the number of failed test cases. Test cases that were aborted due to errors
# before execution count as failed, too.
#
# This script uses some advanced bash constructs you might not be used to. Chances are that you
# will find some explanation at the bottom of this file.


###################################################################################################
# Variables                                                                                       #
###################################################################################################

_DIR="$(dirname "$0")" # Name of the working directory.

TESTS=0     # Counts the number of executed test cases.
FAILED=0    # Counts the number of failed test cases.
WARNINGS=0  # Counts the number of test cases that caused warnings.
ERRORS=0    # Counts the number of test cases that didn't run due to errors.
HIERARCHY=0 # Grows and shrinks with each executed test suite. Used to layout the output.


###################################################################################################
# Helper functions to create nice output                                                          #
###################################################################################################

# Escape color sequences for echo.
NC='\033[0m'
RED='\033[1;31m'
GREEN='\033[1;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'


# The following functions print a given message to the screen. The higher-order functions call
# these functions and redirect the output to variables. Most output is printed to the screen when a
# test case was executed completely and all errors and warnings were collected with the help of
# these helper functions.


# Prints an indentation string multiple times.
#
# Params:
# $1: level of indentation.
print_indent() {
  if [ $# -eq 1 ]; then
    for i in $(seq 1 $1); do
      echo -n "|   "
    done
  fi
}


# Prints an indented block with a prefix and a message. The message may span multiple lines. The
# prefix is printed at the first line and subsequent lines are prefixed with a number of spaces.
#
# Params:
# $1: prefix of the first line.
# $2: number of spaces for the prefix of subsequent lines.
# $3: message to print with indentation.
print_indented_block() {
  local prefix="$1"
  local line=""

  while read -r line; do
    print_indent $HIERARCHY ; echo -e "$prefix $line"
    prefix=""
    for i in $(seq 1 $2); do
      prefix="$prefix "
    done
  done <<< "$3"
}


# Prints a message as an info block.
#
# Params:
# $1: message to print.
print_info() {
  print_indented_block "    ${BLUE}Info:${NC}" 9 "$1"
}


# Prints a message as a warning. If there are older warnings the new warning is printed after them.
#
# Params:
# $1: message to print.
print_warning() {
  local message=$(print_indented_block "    ${YELLOW}WARNING:${NC}" 12 "$1")
  if [ ${#WARNING_MESSAGE} -eq 0 ]; then
    echo -e "$message"
  else
    echo -e "$WARNING_MESSAGE"
    echo -e "$message"
  fi
}


# Prints an overview of how many test cases passed or failed and how many errors and warnings
# occured.
#
# Params:
print_stats() {
  select_color() {
    if [ $1 -eq 0 ]; then echo -ne "$2" ; else echo -ne "$3" ; fi
  }

  local PASSED=$(($TESTS - $FAILED))
  local PERCENT=0 # Percentage of failed test cases.
  local color=""  # The color that is used to print numbers.

  if [ $TESTS -ne 0 ]; then
    PERCENT=$((100 * $FAILED / $TESTS))
  fi

  echo -e "Executed ${BLUE}$TESTS${NC} tests."

  color=$(select_color $FAILED "${NC}" "${RED}")
  echo -ne "Failed: $color$FAILED${NC} ($PERCENT%)"
  echo -ne " - "

  color=$(select_color $PASSED "${GREEN}" "${GREEN}")
  echo -ne "Passed: ${GREEN}$PASSED${NC} ($((100 - $PERCENT))%)"
  echo -ne " - "

  color=$(select_color $WARNINGS "${NC}" "${YELLOW}")
  echo -ne "Warnings: $color$WARNINGS${NC}"
  echo -ne " - "

  color=$(select_color $ERRORS "${NC}" "${RED}")
  echo -ne "Errors: $color$ERRORS${NC}"
  echo ""
}


###################################################################################################
# Helper functions for test execution                                                             #
###################################################################################################

# These variables help to pass messages to the test case executer.
ERROR_MESSAGE=""   # Passes error messages to test case executer.
WARNING_MESSAGE="" # Passes warning messages to test case executer.
STATUS_MESSAGE=""  # Passes status messages (eg: ERROR, OK) to test case executer.


# The following functions perform a specific step of the whole test case execution. For each test
# case they execute one of the test case scripts to initialize, run and cleanup the test case.
# They redirect the output of those scripts. stdout is ignored, but the output of stderr produces
# warnings or errors.


# Checks whether the current directory has all the needed test case files and creates an error.
#
# Params:
# $1: test case directory.
# $?: 0 if test case contains all files, 1 otherwise.
check_test() {
  local RET=0

  if [ ! -x "init.sh" ] || [ ! -x "run.sh" ] || [ ! -x "clean.sh" ] || [ ! -r "on_error.txt" ]; then
    RET=1
    FAILED=$(($FAILED + 1))
    ERRORS=$(($ERRORS + 1))

    local message=""
    read -r -d '' message <<- EOM
	Missing necessary test case files.
	Compare '$1/' with '$_DIR/.template/' and read '$_DIR/readme.md'!
	EOM

    STATUS_MESSAGE="${RED}ERROR${NC}"
    ERROR_MESSAGE=$(print_info "$message")
  fi

  return $RET
}


# Initializes the test case and creates errors and warnings in case of failures.
#
# Params:
# $1: test case directory.
# $?: 0 if initialization script returned no errors and printed nothing to stderr, 1 otherwise.
init_test() {
  local RET=0
  local output=""

  output=$(./init.sh 2>&1 > /dev/null)
  if [ $? -ne 0 ]; then
    RET=1
    FAILED=$(($FAILED + 1))
    ERRORS=$(($ERRORS + 1))

    local message=""
    read -r -d '' message <<- EOM
	'$1/init.sh' returned with error code.
	$output
	EOM

    STATUS_MESSAGE="${RED}ERROR${NC}"
    ERROR_MESSAGE=$(print_info "$message")
  fi

  if [ ${#output} -ne 0 ]; then
    WARNINGS=$(($WARNINGS + 1))

    local message=""
    read -r -d '' message <<- EOM
	'$1/init.sh' produced a warning.
	$output
	EOM

    WARNING_MESSAGE=$(print_warning "$message")
  fi

  return $RET
}


# Executes the test case and creates errors and warnings in case of failures.
#
# Params:
# $1: test case directory.
# $?: 0 if execution script returned no errors and printed nothing to stderr, 1 otherwise.
run_test() {
  local RET=0
  local output=""

  output=$(./run.sh 2>&1 > /dev/null)
  if [ $? -eq 0 ]; then
    STATUS_MESSAGE="${GREEN}OK${NC}"
  else
    RET=1
    FAILED=$(($FAILED + 1))

    local message=$(cat on_error.txt)

    STATUS_MESSAGE="${RED}FAILED${NC}"
    ERROR_MESSAGE=$(print_info "$message")
  fi

  if [ ${#output} -ne 0 ]; then
    WARNINGS=$(($WARNINGS + 1))

    local message=""
    read -r -d '' message <<- EOM
	'$1/run.sh' produced a warning.
	$output
	EOM

    WARNING_MESSAGE=$(print_warning "$message")
  fi

  return $RET
}


# Restores the test case and creates warnings in case of failures.
#
# Params:
# $1: test case directory.
# $?: 0 if cleanup script returned no errors and printed nothing to stderr, 1 otherwise.
clean_test() {
  local RET=0
  local output=""

  output=$(./clean.sh 2>&1 > /dev/null)
  if [ $? -ne 0 ] || [ ${#output} -ne 0 ]; then
    WARNINGS=$(($WARNINGS + 1))

    local message=""
    read -r -d '' message <<- EOM
	'$1/cleanup.sh' failed or/and produced a warning.
	$output
	EOM

    WARNING_MESSAGE=$(print_warning "$message")
  fi

  return $RET
}


###################################################################################################
# Core functions for test suite and test case execution                                           #
###################################################################################################

# Interprets a given directory as test case and executes the test if possible or prints an error
# otherwise. The test case runs with the test case directory as working directory. A test case
# is executed in multiple steps:
# 1. test case initialization with error reporting
# 2. test case execution with reporting the failing or passing
# 3. test case resetting with warning in case of failure
#
# Params:
# $1: the directory of the test case.
execute_test_case() {
  TESTS=$(($TESTS + 1))

  # Reset messages for each test case.
  ERROR_MESSAGE=""
  WARNING_MESSAGE=""
  STATUS_MESSAGE=""

  cd "$1"
  print_indent $HIERARCHY ; echo -n "Execute test \"$1\"..."

  check_test "$1"
  if [ $? -eq 0 ]; then
    init_test "$1"
    if [ $? -eq 0 ]; then
      run_test "$1"
    fi
    clean_test "$1"
  fi

  print_if_not_empty() {
    if [ ${#1} -ne 0 ]; then
      local line=""
      while read -r line; do
        echo -e "$line"
      done <<< "$1"
    fi
  }

  echo -e " - $STATUS_MESSAGE"
  print_if_not_empty "$ERROR_MESSAGE"
  print_if_not_empty "$WARNING_MESSAGE"

  cd ..
}


# Interprets a given directory as test suite and executes the containing test cases if possible or
# prints an error otherwise. Nested test suites are executed recursively.
#
# Params:
# $1: the directory of the test suite.
execute_suite() {
  local new_line=0 # Used to print a new line if a test suite follows a test case.

  cd "$1"
  print_indent $HIERARCHY ; echo "Execute suite \"$1\"..."

  local dir=""
  for dir in ./*; do
    if [ -d "$dir" ]; then
      if [ -f "$dir/.suite" ]; then
        HIERARCHY=$(($HIERARCHY + 1))

        # Print new line if a test case was executed earlier and reset the flag.
        if [ $new_line -eq 1 ]; then
          new_line=0
          print_indent $HIERARCHY ; echo ""
        fi

        execute_suite "$dir"
        HIERARCHY=$(($HIERARCHY - 1))
      elif [ -f "$dir/.test" ]; then
        HIERARCHY=$(($HIERARCHY + 1))
        new_line=1 # Set flag so if a test suite follows a new line is printed.
        execute_test_case "$dir"
        HIERARCHY=$(($HIERARCHY - 1))
      fi
    fi
  done

  print_indent $HIERARCHY ; echo "+-- End of suite \"$1\""
  print_indent $HIERARCHY ; echo ""
  cd ..
}


echo ""
echo "shUnit Tests"
echo "========================="
echo ""

execute_suite "$_DIR"

echo "========================="
print_stats
echo ""

exit $FAILED


###################################################################################################
# Command explanations                                                                            #
###################################################################################################

# 1.
# ```sh
# while read -r line; do
#   echo $line
# done <<< "$string"
# ```
# The `read` command reads an input that is passed by `while` and stores it in the variable `line`.
# The `<<<` iterates over the lines in the variable `string`.

# 2.
# ```sh
# var=$(command)
# ```
# The construct `$(...)` stores the output of a command in a variable.

# 3.
# ```sh
# if [ ${#var} -eq 0 ]
# ```
# The construct `${#...}` calculates the length of a string variable.

# 4.
# ```sh
# read -r -d '' var <<- EOM
# ->  Line 1
# ->  Line 2
# ->  EOM
# ```
# Again the `read` command that can store strings to a variable. In this case multiple subsequent
# lines are stored in `var`. The input is read until the marker `EOM` is reached. All leading tabs
# are ignored due to `-` in the `<<-` construct. The parameter `-p` makes `read` ignore backslashes
# that would be interpreted as escape sequences otherwise. `-d ''` makes the command store multiple
# lines in the variable.

# 5.
# ```sh
# var=$(command 2>&1 > /dev/null)
# ```
# This construct executes a the command and ignores stdout whereas stderr is piped to the variable.
