#!/bin/bash


print_usage() {
  echo ""
  echo "Usage: $0 -h|--help | <dir>"
  echo "Creates a new test suite in the given directory."
  echo ""
}


_DIR="$(dirname "$0")"
RET=0

if [ $# -eq 1 ]; then
  if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    print_usage
  elif [ -d "$1" ]; then
    echo ""
    echo "Directory \"$1\" already exists."
    echo ""
    RET=2
  else
    echo ""
    echo "Create new test suite in directory \"$1\"."

    mkdir -v "$1"
    RET=$(($RET + $?))

    touch "$1/.suite"
    RET=$(($RET + $?))

    touch "$1/readme.md"
    RET=$(($RET + $?))
    echo "Author: $USER" >> "$1/readme.md"
    echo -n "Date:   " >> "$1/readme.md" ; date "+%d.%m.%Y" >> "$1/readme.md"
    echo "" >> "$1/readme.md"
    echo "Describe what the test suite does in this file." >> "$1/readme.md"

    ln -sv "../.template" "$1/.template"
    RET=$(($RET + $?))

    for dir in ./*; do
      if [ -f $dir ] && [ -x $dir ]; then
        ln -sv "../$dir" "$1/$dir"
        RET=$(($RET + $?))
      fi
    done

    echo ""
  fi
else
  print_usage
  RET=1
fi

exit $RET
