#!/bin/bash


print_usage() {
  echo ""
  echo "Usage: $0 -h|--help | <dir>"
  echo "Creates a new test case in the given directory."
  echo ""
}


_DIR="$(dirname "$0")"
RET=0

if [ $# -eq 1 ]; then
  if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    print_usage
  elif [ -d "$1" ]; then
    echo ""
    echo "Directory \"$1\" already exists."
    echo ""
    RET=2
  else
    echo ""
    echo "Create new test case in directory \"$1\"."

    cp -rv "$_DIR/.template/" "$1"
    RET=$(($RET + $?))

    touch "$1/.test"
    RET=$(($RET + $?))

    echo ""
  fi
else
  print_usage
  RET=1
fi

exit $RET
